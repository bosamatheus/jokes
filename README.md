[![pipeline status](https://gitlab.com/bosamatheus/jokes/badges/main/pipeline.svg)](https://gitlab.com/bosamatheus/jokes/-/commits/main) [![coverage report](https://gitlab.com/bosamatheus/jokes/badges/main/coverage.svg)](https://gitlab.com/bosamatheus/jokes/-/commits/main)

# Jokes API

API Proxy that consumes JokeAPI using Golang and Fiber.

## Requirements

- [JokeAPI](https://v2.jokeapi.dev/): a REST API that serves uniformly and well formatted jokes.
- [Fiber](https://gofiber.io/): an Express-inspired web framework written in Go.
- [Go Tools & GitLab](https://medium.com/pantomath/go-tools-gitlab-how-to-do-continuous-integration-like-a-boss-941a3a9ad0b6): how to do Continuous Integration like a boss.
