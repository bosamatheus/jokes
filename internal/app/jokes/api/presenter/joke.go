package presenter

// Joke presenter.
type Joke struct {
	Language string `json:"language"`
	Category string `json:"category"`
	Content  string `json:"joke"`
}
