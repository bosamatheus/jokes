package handler

import (
	"strconv"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/bosamatheus/jokes/internal/app/jokes/api/presenter"
	"gitlab.com/bosamatheus/jokes/internal/app/jokes/usecase/joke"
	"gitlab.com/bosamatheus/jokes/internal/pkg/util"
)

// MakeJokeHandlers creates handlers for the Jokes API.
func MakeJokeHandlers(app *fiber.App, service joke.UseCase) {
	app.Get("/jokes/:category", func(c *fiber.Ctx) error {
		return findJokes(c, service)
	})
}

// findJokes finds jokes in the JokeAPI.
func findJokes(c *fiber.Ctx, service joke.UseCase) error {
	size, err := strconv.Atoi(c.Query("size"))
	if err != nil {
		return fiber.NewError(fiber.StatusBadRequest, err.Error())
	}

	query := joke.Query{
		Size:     size,
		Category: c.Params("category"),
		Contains: c.Query("contains"),
	}
	jokes, err := service.FindJokes(&query)
	if err != nil {
		return err
	}

	results := make([]presenter.Joke, len(*jokes))
	for i := range results {
		results[i] = presenter.Joke{
			Language: util.LanguageAcronymToFull((*jokes)[i].Lang),
			Category: (*jokes)[i].Category,
			Content:  (*jokes)[i].Content,
		}
	}
	return c.JSON(results)
}
