package repository

import (
	"gitlab.com/bosamatheus/jokes/internal/app/jokes/entity"
)

// Params data.
type Params struct {
	Size     int
	Category string
	Contains string
}

type jokesResponse struct {
	Jokes []entity.Joke `json:"jokes"`
}

// JokeAPIRepository repository.
type JokeAPIRepository struct {
	baseURL string
}

// NewJokeAPIRepository create new repository.
func NewJokeAPIRepository(baseURL string) *JokeAPIRepository {
	return &JokeAPIRepository{
		baseURL: baseURL,
	}
}

// Find jokes.
func (r *JokeAPIRepository) Find(p *Params) (*[]entity.Joke, error) {
	var e jokeAPIExecutor
	if p.Contains == "" {
		e = newJokeAPIList(r.baseURL, p)
	} else {
		e = newJokeAPISearch(r.baseURL, p)
	}
	return e.execute()
}
