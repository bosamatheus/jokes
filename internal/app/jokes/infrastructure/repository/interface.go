package repository

import (
	"gitlab.com/bosamatheus/jokes/internal/app/jokes/entity"
)

// jokeAPIExecutor interface.
type jokeAPIExecutor interface {
	execute() (*[]entity.Joke, error)
}
