package repository

import (
	"encoding/json"
	"fmt"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/bosamatheus/jokes/internal/app/jokes/entity"
)

// jokeAPIList executor.
type jokeAPIList struct {
	baseURL  string
	size     int
	category string
}

// newJokeAPIList create new list executor.
func newJokeAPIList(baseURL string, p *Params) *jokeAPIList {
	return &jokeAPIList{
		baseURL:  baseURL,
		size:     p.Size,
		category: p.Category,
	}
}

// execute jokes list.
func (r *jokeAPIList) execute() (*[]entity.Joke, error) {
	uri := fmt.Sprintf("%s/joke/%s?amount=%d",
		r.baseURL,
		r.category,
		r.size,
	)

	a := fiber.AcquireAgent()
	req := a.Request()
	req.Header.SetMethod(fiber.MethodGet)
	req.SetRequestURI(uri)
	if err := a.Parse(); err != nil {
		panic(err)
	}

	code, body, errs := a.Bytes()
	if code != 200 {
		err := fmt.Errorf("an error has occurred while getting jokes: %s", errs)
		return nil, err
	}

	var response jokesResponse
	if err := json.Unmarshal(body, &response); err != nil {
		err = fmt.Errorf("an error has occurred while unmarshalling jokes: %s", err)
		return nil, err
	}

	return &response.Jokes, nil
}
