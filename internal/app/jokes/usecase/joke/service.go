package joke

import (
	"gitlab.com/bosamatheus/jokes/internal/app/jokes/entity"
	"gitlab.com/bosamatheus/jokes/internal/app/jokes/infrastructure/repository"
)

// Query data.
type Query struct {
	Size     int
	Category string
	Contains string
}

// Service interface.
type Service struct {
	repo Repository
}

// NewService create new use case.
func NewService(r Repository) *Service {
	return &Service{
		repo: r,
	}
}

// FindJokes list jokes.
func (s *Service) FindJokes(q *Query) (*[]entity.Joke, error) {
	p := repository.Params{
		Size:     q.Size,
		Category: q.Category,
		Contains: q.Contains,
	}
	return s.repo.Find(&p)
}
