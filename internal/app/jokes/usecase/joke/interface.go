package joke

import (
	"gitlab.com/bosamatheus/jokes/internal/app/jokes/entity"
	"gitlab.com/bosamatheus/jokes/internal/app/jokes/infrastructure/repository"
)

// Reader interface.
type Reader interface {
	Find(p *repository.Params) (*[]entity.Joke, error)
}

// Repository interface.
type Repository interface {
	Reader
}

// UseCase interface.
type UseCase interface {
	FindJokes(q *Query) (*[]entity.Joke, error)
}
