package entity

// Joke entity.
type Joke struct {
	ID       int64           `json:"id"`
	Category string          `json:"category"`
	Type     string          `json:"type"`
	Content  string          `json:"joke"`
	Lang     string          `json:"lang"`
	Flags    map[string]bool `json:"flags"`
}
