package util

import "testing"

func TestLanguageAcronymToFull(t *testing.T) {
	type args struct {
		acronym string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{"CS to full", args{"cs"}, "Czech"},
		{"DE to full", args{"de"}, "German"},
		{"EN to full", args{"en"}, "English"},
		{"ES to full", args{"es"}, "Spanish"},
		{"FR to full", args{"fr"}, "French"},
		{"PT to full", args{"pt"}, "Portuguese"},
		{"Another language", args{"ru"}, "Unknown"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := LanguageAcronymToFull(tt.args.acronym); got != tt.want {
				t.Errorf("LanguageAcronymToFull() = %v, want %v", got, tt.want)
			}
		})
	}
}
