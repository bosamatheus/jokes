package util

// LanguageAcronymToFull is a map of language acronyms to full language names.
func LanguageAcronymToFull(acronym string) string {
	switch acronym {
	case "cs":
		return "Czech"
	case "de":
		return "German"
	case "en":
		return "English"
	case "es":
		return "Spanish"
	case "fr":
		return "French"
	case "pt":
		return "Portuguese"
	default:
		return "Unknown"
	}
}
