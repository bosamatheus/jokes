package main

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/bosamatheus/jokes/configs"
	"gitlab.com/bosamatheus/jokes/internal/app/jokes/api/handler"
	"gitlab.com/bosamatheus/jokes/internal/app/jokes/infrastructure/repository"
	"gitlab.com/bosamatheus/jokes/internal/app/jokes/usecase/joke"
)

func main() {
	jokeRepo := repository.NewJokeAPIRepository(configs.JokeAPIBaseURL)
	jokeService := joke.NewService(jokeRepo)

	app := fiber.New()
	handler.MakeJokeHandlers(app, jokeService)
	app.Listen(configs.ServerPort)
}
