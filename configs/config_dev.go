package configs

// Configuration for the development environment.
const (
	ServerPort     = ":3000"
	JokeAPIBaseURL = "https://v2.jokeapi.dev"
)
